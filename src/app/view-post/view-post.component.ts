import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, getMatTooltipInvalidPositionError } from '@angular/material';
import { HttpService } from '../services/http.service'
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css']
})
export class ViewPostComponent implements OnInit {
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['postTitle', 'content', 'tags', 'createdBy', 'actions'];
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  searchKey: string;
  postId: string;
  postName: string
  constructor(private http: HttpService,
    private route: Router) { }

  ngOnInit() {
    this.getAllPost()
  }
  getAllPost() {
    this.http.getAllPost().subscribe(
      list => {
        console.log("data in list=====>", list);

        let array = list.post.map(item => {
          return {
            $key: item._id,
            postTitle: item.postTitle,
            content: item.content,
            tags: item.tags ? item.tags : 'NA',
            createdBy: item.userId ? item.userId.username : 'NA'
          };
        });
        this.listData = new MatTableDataSource(array);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
      }
    );
  }
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  getpostId(rowData) {

    this.postId = rowData.$key
    this.postName = rowData.postTitle
    console.log("rowData====>", this.postId);

  }

  deletePost() {
    this.http.deletePost(this.postId).subscribe(
      (resp) => {
        document.getElementById("exampleModal").click();
        this.getAllPost()

      },
      (error) => {
        console.log(error);
      }
    )
  }
  navigateCreate() {
    this.route
      .navigate([`../post/`])
  }

  logout() {
    console.log("logout called");

    localStorage.clear()
    this.route.navigate([`../login/`])
  }
  navigateHome() {
    this.route.navigate([`../post-View-without-login/`])
  }
}

