import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from '../services/http.service'
import { Router, ActivatedRoute } from '@angular/router';
export interface Fruit {
  name: string;
}
@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  postObj: any = {}
  a: string
  name: string
  enteredText = ''
  newPost = ''
  hide = true;
  constructor(private snackBar: MatSnackBar,
    private http: HttpService,
    private route: Router) { }
  buttonCalled() {
    this.newPost = this.enteredText
    this.snackBar.open(this.enteredText);
  }

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  fruits: Fruit[] = [
    { name: 'New Post' }
  ];

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;


    if ((value || '').trim()) {
      this.fruits.push({ name: value.trim() });
    }

    if (input) {
      input.value = '';
    }
  }

  remove(fruit: Fruit): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }
  ngOnInit() {
  }
  addPost() {
    let finalTag = this.fruits.map(x => {
      return x.name
    })
    console.log("finalTag=====>", finalTag);

    let postObj = {
      postTitle: this.postObj.enteredText,
      content: this.postObj.postContent,
      tags: finalTag,
      userId: localStorage.getItem('userId')
    }
    console.log("postObj====>", postObj);


    this.http.addPost(postObj).subscribe(
      (resp) => {
        console.log("res of post=====>😺", resp);
        this.route
          .navigate([`../view/`])
      },
      (err) => {
        alert(err)
        console.log(err);


      })
  }

  navigatePost() {
    this.route
      .navigate([`../view/`])
  }

  logout() {
    console.log("logout called");

    localStorage.clear()
    this.route.navigate([`../login/`])
  }
  navigateSpan() {
    this.route.navigate([`../post-View-without-login/`])
  }
}
