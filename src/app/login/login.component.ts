import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { HttpService } from '../services/http.service'
import { Router, ActivatedRoute } from '@angular/router';
import { interval } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  a: string
  name: string
  progressCount
  enteredText = ''
  newPost = ''
  disableLogin = false

  progressbarValue = 100;
  curSec: number = 0;
  hide = true;
  showSpinner = false
  loginObj: any = {}
  constructor(private snackBar: MatSnackBar,
    private http: HttpService,
    private route: Router) {
    // setTimeout(() => {



    //   {
    //     const time = this.seconds;
    //     const timer$ = interval(1000);

    //     const sub = timer$.subscribe((sec) => {
    //       this.progressbarValue = 100 - sec * 100 / this.seconds;
    //       this.curSec = sec;

    //       if (this.curSec === this.seconds) {
    //         sub.unsubscribe();
    //       }
    //     });
    //   }
    // }, 5000);
  }
  buttonCalled() {
    this.newPost = this.enteredText
    this.snackBar.open(this.enteredText);
  }
  startTimer(seconds: number) {
    this.disableLogin = true
    console.log("secods===>", seconds);

    const time = seconds;
    const timer$ = interval(1000);

    const sub = timer$.subscribe((sec) => {
      this.progressbarValue = 100 - sec * 100 / seconds;
      this.curSec = sec;

      if (this.curSec === seconds) {
        this.curSec = 0
        sub.unsubscribe();
        this.disableLogin = false
      }
    });
  }

  ngOnInit() {
  }

  userLogin() {
    this.startTimer(3)
    this.showSpinner = true
    console.log("functions called====>", this.showSpinner);
    let postObj = {
      username: this.loginObj.loginUsername,
      password: this.loginObj.loginPassword
    }
    console.log("postObj====>", postObj);


    this.http.userLogin(postObj).subscribe(

      (resp) => {
        if (resp.message) {
          this.snackBar.open(resp.message, "dismiss", { duration: 5000 });
        }
        this.showSpinner = false
        console.log("res of post=====>😺", resp);
        localStorage.setItem("userId", resp.result._id);

        this.route
          .navigate([`../post-View-without-login/`])
      },
      (err) => {
        console.log(err);
        this.showSpinner = false
        this.snackBar.open(err.message, "dismiss", { duration: 5000 });
      })
  }

  userRegister() {
    let postObj = {
      email: this.loginObj.addEmail,
      username: this.loginObj.addUsername,
      password: this.loginObj.addPassword
    }

    this.http.registerUser(postObj).subscribe(
      (resp) => {
        console.log("res of post=====>😺", resp);
        this.snackBar.open("You have successfully registered now you can login", "dismiss", { duration: 5000 });
        document.getElementById("registerModal").click();

      },
      (err) => {
        this.snackBar.open(err.error.message, "dismiss", { duration: 10000 });
        console.log(err);
      })

  }
}
