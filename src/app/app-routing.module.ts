import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../app/login/login.component'
import { CreatePostComponent } from '../app/create-post/create-post.component'
import { ViewPostComponent } from './view-post/view-post.component';
import { ViewPostWithoutLoginComponent } from './view-post-without-login/view-post-without-login.component';
const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'post',
    component: CreatePostComponent
  },
  {
    path: 'view',
    component: ViewPostComponent
  },
  {
    path: 'post-View-without-login',
    component: ViewPostWithoutLoginComponent
  },

  {
    path: '',
    redirectTo: '/post-View-without-login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
