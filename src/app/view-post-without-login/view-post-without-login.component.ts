import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service'
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material'
import { DialogQuotesComponent } from '../dialog-quotes/dialog-quotes.component';
@Component({
  selector: 'app-view-post-without-login',
  templateUrl: './view-post-without-login.component.html',
  styleUrls: ['./view-post-without-login.component.css']
})
export class ViewPostWithoutLoginComponent implements OnInit {
  panelOpenState = false;
  rows: any = []
  randomQuoteCount: string
  quoteClicked = false
  randomPost: string
  loginLocal: string
  readyRow: string
  randomFinalQuotes: string
  constructor(private http: HttpService,
    private route: Router, public dialog: MatDialog) { }

  ngOnInit() {

    this.getallQuotes()
    this.getAllPost()
    this.getQuotes()

  }


  getAllPost() {
    this.http.getAllPost().subscribe(data => {

      let postArr = []
      console.log("data=====>", data);

      data.post.map(element => {

        postArr.push(
          {
            'postTitle': element.postTitle,
            'content': element.content,
            'createdBy': element.userId.username,
            'tag': element.tags,
            'postId': element._id,
            'readBy': element.readBy ? element.readBy : null
          })
      })
      this.rows = postArr
      console.log("rows====>", this.rows);

    }, err => {
      console.log(err);
    })
  }

  loginNavigate() {
    this.route.navigate([`../login/`])
  }

  openDialog() {
    let userId = localStorage.getItem('userId');
    let quoteCount = localStorage.getItem('quoteCount');

    if (quoteCount == null) {
      if (userId == null) {
        let dialogRef = this.dialog.open(DialogQuotesComponent, { data: { name: this.randomFinalQuotes } });
      }
      if (userId != null) {
        let dialogRef = this.dialog.open(DialogQuotesComponent, { data: { name: this.randomFinalQuotes } });
        this.randomQuoteCount = '1'
        localStorage.setItem('quoteCount', this.randomQuoteCount)
      }
      // dialogRef.afterClosed().subscribe(result => {
      //   console.log("result====>", result);

      // })
    }
    if (localStorage.getItem('quoteCount') != null) {
      return false
    }



  }


  async getallQuotes() {
    try {

      let quotes = await fetch("https://type.fit/api/quotes");
      let finalQuotes = await quotes.json()
      let value = (Math.random())
      let finalValue = value.toFixed(4).toString()
      let convertToSlice = finalValue.slice(-4)
      if (convertToSlice >= '1643') {
        let greaterValue = convertToSlice.slice(0, 3)
        this.randomFinalQuotes = finalQuotes[greaterValue]
      }
      if (convertToSlice.slice(0, 1).toString() == '0') {
        let sliceString = convertToSlice.slice(1, 3)
        this.randomFinalQuotes = finalQuotes[sliceString]
      }
      if (convertToSlice.slice(0, 1).toString() != '0' && convertToSlice <= '1643') {
        this.randomFinalQuotes = finalQuotes[convertToSlice]
      }
      this.openDialog()
    } catch (error) {
      console.log(error);
    }


  }
  viewNavigate() {
    this.route.navigate([`../view/`])
  }
  createNavigate() {
    this.route.navigate([`../post/`])
  }
  getQuotes() {
    this.loginLocal = localStorage.getItem('userId')
    console.log("quotes outside function 🚛", this.loginLocal);

  }
  logout() {
    console.log("logout called");

    localStorage.clear()
    this.route.navigate([`../login/`])
  }

  expansionClick(rowData) {

    this.randomPost = rowData.postTitle
    this.quoteClicked = true
    console.log("row======>", rowData);
    if (rowData.readBy === null) {
      let finalObj = {
        readBy: localStorage.getItem('userId')
      }
      let postId = rowData.postId

      this.http.readByPost(postId, finalObj).subscribe(
        (resp) => {

          console.log("resp====>", resp);


        },
        (error) => {
          console.log(error);
        }
      )
    }



  }
}
