import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPostWithoutLoginComponent } from './view-post-without-login.component';

describe('ViewPostWithoutLoginComponent', () => {
  let component: ViewPostWithoutLoginComponent;
  let fixture: ComponentFixture<ViewPostWithoutLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPostWithoutLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPostWithoutLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
