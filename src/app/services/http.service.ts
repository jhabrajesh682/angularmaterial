import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private masterApiUrl;
  private roomUrl;
  private baseV1 = 'v1/';

  constructor(private _http: HttpClient) {
    this.masterApiUrl = 'http://localhost:8090/api/';
    this.roomUrl = 'http://localhost:8090/'
  }

  private getHeaderWithoutToken() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return httpOptions;
  }
  addPost(finalObj) {
    console.log("finalObj====>", finalObj);
    return this._http.post<any>(
      this.masterApiUrl + this.baseV1 + `post/create`,
      finalObj
    );
  }

  userLogin(finalObj) {
    return this._http.post<any>(
      this.roomUrl + `join`,
      finalObj
    );
  }

  getAllPost() {
    return this._http.get<any>(
      this.masterApiUrl + this.baseV1 + `post`
    );
  }

  deletePost(id) {

    return this._http.delete<any>(
      this.masterApiUrl + this.baseV1 + `post/${id}`,
    );
  }

  registerUser(finalObj) {
    return this._http.post<any>(
      this.roomUrl + `register`,
      finalObj
    );
  }


  readByPost(id, finalObj) {

    return this._http.put<any>(
      this.masterApiUrl + this.baseV1 + `post/${id}`, finalObj
    );
  }
}
