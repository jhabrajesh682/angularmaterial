import { BrowserModule } from '@angular/platform-browser';
import { Component, Inject } from '@angular/core';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material/material.module'
import { MatSnackBarModule } from "@angular/material";
import { MAT_SNACK_BAR_DATA } from '@angular/material';
import { CreatePostComponent } from './create-post/create-post.component';
import { LoginComponent } from './login/login.component';
import { MatSelectModule } from '@angular/material/select';
import { ViewPostComponent } from './view-post/view-post.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatChipsModule } from '@angular/material/chips';
import { HttpClientModule } from '@angular/common/http';
import { ViewPostWithoutLoginComponent } from './view-post-without-login/view-post-without-login.component';
import { DialogQuotesComponent } from './dialog-quotes/dialog-quotes.component';
@NgModule({
  declarations: [
    AppComponent,
    CreatePostComponent,
    LoginComponent,
    ViewPostComponent,
    ViewPostWithoutLoginComponent,
    DialogQuotesComponent
  ],
  entryComponents: [
    DialogQuotesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    MatSnackBarModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatChipsModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
