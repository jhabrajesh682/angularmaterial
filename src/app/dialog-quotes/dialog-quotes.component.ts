import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-quotes',
  templateUrl: './dialog-quotes.component.html',
  styleUrls: ['./dialog-quotes.component.css']
})
export class DialogQuotesComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    console.log("data=====>", this.data);

  }

}
